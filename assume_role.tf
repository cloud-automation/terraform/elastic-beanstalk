provider "aws" {
  region        = "${var.region}"
  version       = "1.51.0" 
  assume_role {
    role_arn    = "${var.assume_role}"
  }
}
