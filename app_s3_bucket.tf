# Application source S3 bucket
# https://www.terraform.io/docs/providers/aws/r/s3_bucket.html

resource "aws_s3_bucket" "elasticbeanstalk-bucket" {
  bucket = "elasticbeanstalk-mycompany-${var.region}-${var.CnamePrefix}"
  tags                   = "${var.tags}"
  versioning {
    enabled = true
  }
  force_destroy = true
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        # kms_master_key_id = "${aws_kms_key.mykey.arn}"
        # sse_algorithm     = "aws:kms"
        sse_algorithm     = "AES256"
      }
    }
   }
  }

# Upload the application bundle to S3 bucket
resource "aws_s3_bucket_object" "eb_app_bundle" {
  # depends_on             = ["aws_s3_bucket.elasticbeanstalk-bucket"]
  bucket                 = "${aws_s3_bucket.elasticbeanstalk-bucket.id}"
  # server_side_encryption = "${var.server_side_encryption}"
  # storage_class          = "${var.storage_class}"
  # tags                   = "${var.default_tags}"
  key                    = "${var.eb_app_key}"
  source                 = "${var.eb_app_key}"
  tags                   = "${var.tags}"
}