resource "aws_acm_certificate" "cert" {
  domain_name = "${var.CI_PROJECT_NAME}.${var.domain_name}"
  tags = {
    Name = "${var.CI_PROJECT_NAME}.${var.domain_name}"
  }
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "zone" {
  name         = "${var.domain_name}."
  private_zone = false
}

resource "aws_route53_record" "cert" {
  name    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.zone.id}"
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = "${aws_acm_certificate.cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert.fqdn}"]
}

resource "aws_route53_record" "my_domain" {
  name    = "${var.CI_PROJECT_NAME}"
  type    = "A"
  zone_id = "${data.aws_route53_zone.zone.id}"

  alias {
    name = "${aws_elastic_beanstalk_environment.beanstalk_env.cname}"
    zone_id = "${data.aws_elastic_beanstalk_hosted_zone.beanstalk_env.id}"
    evaluate_target_health = false
  }
}

data "aws_elastic_beanstalk_hosted_zone" "beanstalk_env" {}