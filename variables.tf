# Variable definitions go here

variable "region" {
  description = "AWS Region where we place resources"
  default     = "eu-west-1"
}

# Assume this role to deploy. Can be cross-account to deploy in other accounts!
variable "assume_role" {
  description = "AWS IAM role ARN to assume"
  default     = ""
}

variable "tags" {
  type        = "map"
  description = "All neccecary tags"
  default     = {}
}

###############################################################################
##                                                                           ##
## The settings below is automatically extracted from the GitLab environment ##
##                                                                           ##
###############################################################################

# The project folder name is used to define the EB application name
variable "CI_PROJECT_NAME" {
  description = "CI_PROJECT_NAME inherited from GitLab runner env"
  # default     = ""
}

# Use project path to set a unique state path in S3 backend bucket
variable "CI_PROJECT_PATH" {
    description = "The GitLAB project name that is currently being built (actually it is project folder name)"
}

# So we know what branch of the repo was committed to.
variable "CI_COMMIT_REF_NAME" {
  description = "Git branch name"
  default     = ""
}

# So we know exactly what version of the code was committed (used to set tags)
variable "CI_COMMIT_SHA" {
  description = "The commit revision for which project is built"
  default     = ""
}

variable "eb_app_key" {
  description = "Elastic Beanstalk Application bundle"
  default = ""
}

variable "SolutionStackName" {
  description = ""
  default = "64bit Amazon Linux 2018.03 v2.8.3 running PHP 7.2"
}

variable "CnamePrefix" {
  description = "CNAME for application prefix"
  default =""
}


# variable "env_vars" {
#   description = "Environment variables"
#   #default =""
#   type = "list"
# }

# VPC
# variable "VPCId" {
#   description = "vpc-d0ef3eb6"
#   default = ""
# }

# variable "Subnets" {
#   description = "Use only private subnets for EC2"
#   default = ""
# }
# variable "ELBSubnets" {
#   description = "Use only private subnets for Load Balancer"
#   default = ""
# }
variable "ELBScheme" {
  description = "Use only internal loadbalancer"
  default = "internal"
}
variable "AssociatePublicIpAddress" {
  description = "Associate Public IP address"
  default = "false"
}

# LoadBalancer

    ################### Load Balancer Settings ###################
variable "LoadBalancerType" {
  description = "Load Balancer Type"
  default = "application"
  }
variable "ListenerEnabled" {
  description = "Enable listener"
  default = ""
  }
variable "Protocol" {
  description = "HTTP or HTTPS"
  default = ""
}

# variable "loadbalancer_certificate_arn" {
#   description = "loadbalancer certificate arn"
#   default = ""
# }

variable "healthcheck_path" {
  description = "path to the healthcheck"
  default = "/"
}
################### Auto Scaling Settings ########################
variable "InstanceType" {
  description = "Instance Type"
  default = ""
  }

variable "MinSize" {
  default = ""
  }

variable "MaxSize" {
  default = ""
 }

# SSH configuration
variable "keypair" {
  description  = "SSH key pair name"
  default = "iuah-shared-keypair"
 }

variable "ssh_listener_port" {
  description  = "SSH port"
  default = "22"
 }

 
variable "ssh_listener_enabled" {
  description  = "Enable ssh port (true/false)"
  default = "false"
 }
variable "ssh_source_restriction" {
  description  = "Enable ssh port from this source network. Defaults to SCMP."
  default = "138.106.77.0/24"
 }

 # DNS / ROUTE53

 variable "domain_name" {
  description  = "for test: test.mydomain.com, for prod: mydomain.com, add into tfvars"
  default = ""
 }