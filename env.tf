# this file is to create the environment and it is the second step in Elastic Beanstalk
resource "aws_elastic_beanstalk_environment" "beanstalk_env" {
   name                = "${var.CI_PROJECT_NAME}"                                   # Environment name
   application         = "${var.CI_PROJECT_NAME}"                                   # Refering to the Env name
   solution_stack_name = "${var.SolutionStackName}"                                 # Type of plattform
   cname_prefix        = "${var.CnamePrefix}"                                       # Domain name
   poll_interval       = "59s"                                                     # The time between polling the AWS API to check if changes have been applied. Use this to adjust the rate of API calls for any create or update action. Minimum 10s, maximum 180s"
   tier                = "WebServer"                                                # Env tier
   version_label       = "${aws_elastic_beanstalk_application_version.elasticbeanstalk-appversion.name}"
   tags                = "${var.tags}"
   # because of https://github.com/terraform-providers/terraform-provider-aws/issues/3963
  #  lifecycle {
  #   ignore_changes = ["tags"]
  #  }
    ################### VPC Settings ###################
    setting {
    namespace = "aws:ec2:vpc:"
    name      = "VPCId"
    #value     = "${var.VPCId}"
    value     = "${data.aws_cloudformation_stack.gov-vpc.outputs["VPCID"]}"
   }
    setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    #value     = "${var.Subnets}"
    value     = "${data.aws_cloudformation_stack.gov-vpc.outputs["PrivateSubnet1AID"]},${data.aws_cloudformation_stack.gov-vpc.outputs["PrivateSubnet1BID"]},${data.aws_cloudformation_stack.gov-vpc.outputs["PrivateSubnet1CID"]}"
   }
    setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = "${data.aws_cloudformation_stack.gov-vpc.outputs["PrivateSubnet1AID"]},${data.aws_cloudformation_stack.gov-vpc.outputs["PrivateSubnet1BID"]},${data.aws_cloudformation_stack.gov-vpc.outputs["PrivateSubnet1CID"]}"
   }
    setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = "${var.ELBScheme}"
   }
    setting {
    namespace = "aws:ec2:vpc:"
    name      = "AssociatePublicIpAddress"
    value     = "${var.AssociatePublicIpAddress}"
   }
################### Load Balancer Settings ###################
    setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = "${var.LoadBalancerType}"
  }
    # setting {
    # namespace = "aws:elasticbeanstalk:environment"
    # name      = "LoadbalancerType"
    # value     = "internal"
    # }
    ### COMMENT 80 listener
  #   setting {
  #   namespace = "aws:elbv2:listener:80"
  #   name = "ListenerEnabled"
  #   #value = "${var.ListenerEnabled}"
  #   value     = "${aws_acm_certificate.cert.arn == "" ? "false" : "true"}"
  # }
  #   setting {
  #   namespace = "aws:elbv2:listener:80"
  #   name = "Protocol"
  #   value = "${var.Protocol}"
  # }

  #   setting {
  #   namespace = "aws:elbv2:listener:80"
  #   name = "rule_name"
  #   value = "${aws_lb_listener_rule.http_to_https.name}"
  # }

################### Auto Scaling Settings ###################
    setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateType"
    value     = "Health"
  }
    setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "InstanceType"
    value = "${var.InstanceType}"
  }
    setting {
    namespace = "aws:autoscaling:asg"
    name = "MinSize"
    value = "${var.MinSize}"
  }
    setting {
    namespace = "aws:autoscaling:asg"
    name = "MaxSize"
    value = "${var.MaxSize}"
 }
#   ###=========================== Autoscale trigger ========================== ###

#   setting {
#     namespace = "aws:autoscaling:trigger"
#     name      = "MeasureName"
#     value     = "${var.autoscale_measure_name}"
#   }
#   setting {
#     namespace = "aws:autoscaling:trigger"
#     name      = "Statistic"
#     value     = "${var.autoscale_statistic}"
#   }
#   setting {
#     namespace = "aws:autoscaling:trigger"
#     name      = "Unit"
#     value     = "${var.autoscale_unit}"
#   }
#   setting {
#     namespace = "aws:autoscaling:trigger"
#     name      = "LowerThreshold"
#     value     = "${var.autoscale_lower_bound}"
#   }
#   setting {
#     namespace = "aws:autoscaling:trigger"
#     name      = "LowerBreachScaleIncrement"
#     value     = "${var.autoscale_lower_increment}"
#   }
#   setting {
#     namespace = "aws:autoscaling:trigger"
#     name      = "UpperThreshold"
#     value     = "${var.autoscale_upper_bound}"
#   }
#   setting {
#     namespace = "aws:autoscaling:trigger"
#     name      = "UpperBreachScaleIncrement"
#     value     = "${var.autoscale_upper_increment}"
#   }

#   ###=========================== Autoscale trigger ========================== ###

#   setting {
#     namespace = "aws:autoscaling:launchconfiguration"
#     name      = "SecurityGroups"
#     value     = "${aws_security_group.default.id}"
#   }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SSHSourceRestriction"
    value     = "tcp,22,22,${var.ssh_source_restriction}"
  }
#   setting {
#     namespace = "aws:autoscaling:launchconfiguration"
#     name      = "InstanceType"
#     value     = "${var.instance_type}"
#   }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    # value     = "ManagedServicesRole"
    value     = "${aws_iam_instance_profile.ec2-role.name}"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = "${var.keypair}"
  }
#   setting {
#     namespace = "aws:autoscaling:launchconfiguration"
#     name      = "RootVolumeSize"
#     value     = "${var.root_volume_size}"
#   }
#   setting {
#     namespace = "aws:autoscaling:launchconfiguration"
#     name      = "RootVolumeType"
#     value     = "${var.root_volume_type}"
#   }
#   setting {
#     namespace = "aws:autoscaling:asg"
#     name      = "Availability Zones"
#     value     = "${var.availability_zones}"
#   }
#   setting {
#     namespace = "aws:autoscaling:asg"
#     name      = "MinSize"
#     value     = "${var.autoscale_min}"
#   }
#   setting {
#     namespace = "aws:autoscaling:asg"
#     name      = "MaxSize"
#     value     = "${var.autoscale_max}"
#   }
#   setting {
#     namespace = "aws:elb:loadbalancer"
#     name      = "CrossZone"
#     value     = "true"
# #   }
#   setting {
#     namespace = "aws:elb:loadbalancer"
#     name      = "SecurityGroups"
#     value     = "${join(",", var.loadbalancer_security_groups)}"
#   }
#   setting {
#     namespace = "aws:elb:loadbalancer"
#     name      = "ManagedSecurityGroup"
#     value     = "${var.loadbalancer_managed_security_group}"
#   }
#   setting {
#     namespace = "aws:elb:listener"
#     name      = "ListenerProtocol"
#     value     = "HTTP"
#   }
#   setting {
#     namespace = "aws:elb:listener"
#     name      = "InstancePort"
#     value     = "80"
#   }
#   setting {
#     namespace = "aws:elb:listener"
#     name      = "ListenerEnabled"
#     value     = "${var.http_listener_enabled  == "true" || var.loadbalancer_certificate_arn == "" ? "true" : "false"}"
#   }
#   setting {
#     namespace = "aws:elb:listener:443"
#     name      = "ListenerProtocol"
#     value     = "HTTPS"
#   }
  # setting {
  #   namespace = "aws:elb:listener:443"
  #   name      = "InstancePort"
  #   value     = "80"
  # }
  # setting {
  #   namespace = "aws:elb:listener:443"
  #   name      = "SSLCertificateId"
  #   value     = "${aws_acm_certificate.cert.arn == "" ? "false" : "true"}"
  # }
  # setting {
  #   namespace = "aws:elb:listener:443"
  #   name      = "ListenerEnabled"
  #   value     = "${aws_acm_certificate.cert.arn == "" ? "false" : "true"}"
  # }
  setting {
    namespace = "aws:elb:listener:${var.ssh_listener_port}"
    name      = "ListenerProtocol"
    value     = "TCP"
  }
  setting {
    namespace = "aws:elb:listener:${var.ssh_listener_port}"
    name      = "InstancePort"
    value     = "22"
  }
    setting {
      namespace = "aws:elb:listener:${var.ssh_listener_port}"
      name      = "ListenerEnabled"
      value     = "${var.ssh_listener_enabled}"
    }
  setting {
    namespace = "aws:elb:policies"
    name      = "ConnectionSettingIdleTimeout"
    value     = "${var.ssh_listener_enabled == "true" ? "3600" : "60"}"
  }
  setting {
    namespace = "aws:elb:policies"
    name      = "ConnectionDrainingEnabled"
    value     = "true"
  }
#   setting {
#     namespace = "aws:elbv2:loadbalancer"
#     name      = "AccessLogsS3Bucket"
#     value     = "${aws_s3_bucket.elb_logs.id}"
#   }
#   setting {
#     namespace = "aws:elbv2:loadbalancer"
#     name      = "AccessLogsS3Enabled"
#     value     = "true"
#   }
#   setting {
#     namespace = "aws:elbv2:loadbalancer"
#     name      = "SecurityGroups"
#     value     = "${join(",", var.loadbalancer_security_groups)}"
#   }
#   setting {
#     namespace = "aws:elbv2:loadbalancer"
#     name      = "ManagedSecurityGroup"
#     value     = "${var.loadbalancer_managed_security_group}"
#   }
  # setting {
  #   namespace = "aws:elbv2:listener:default"
  #   name      = "ListenerEnabled"
  #   value     = "${var.http_listener_enabled == "true" || var.loadbalancer_certificate_arn == "" ? "true" : "false"}"
  # }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "ListenerEnabled"
    value     = "${aws_acm_certificate.cert.arn == "" ? "false" : "true"}"
  }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "Protocol"
    value     = "HTTPS"
  }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "SSLCertificateArns"
    value     = "${aws_acm_certificate.cert.arn}"
  }

#   setting {
#     namespace = "aws:elbv2:listener:443"
#     name      = "SSLPolicy"
#     value     = "${var.loadbalancer_type == "application" ? var.loadbalancer_ssl_policy : ""}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:healthreporting:system"
#     name      = "ConfigDocument"
#     value     = "${var.config_document}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:application"
#     name      = "Application Healthcheck URL"
#     value     = "HTTP:80${var.healthcheck_url}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:environment"
#     name      = "LoadBalancerType"
#     value     = "${var.loadbalancer_type}"
#   }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${aws_iam_role.service.name}"
  }
#   setting {
#     namespace = "aws:elasticbeanstalk:healthreporting:system"
#     name      = "SystemType"
#     value     = "enhanced"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:command"
#     name      = "BatchSizeType"
#     value     = "Fixed"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:command"
#     name      = "BatchSize"
#     value     = "1"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:application:environment"
#     name      = "BASE_HOST"
#     value     = "${var.name}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:application:environment"
#     name      = "CONFIG_SOURCE"
#     value     = "${var.config_source}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:managedactions"
#     name      = "ManagedActionsEnabled"
#     value     = "${var.enable_managed_actions ? "true" : "false"}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:managedactions"
#     name      = "PreferredStartTime"
#     value     = "${var.preferred_start_time}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
#     name      = "UpdateLevel"
#     value     = "${var.update_level}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:managedactions:platformupdate"
#     name      = "InstanceRefreshEnabled"
#     value     = "${var.instance_refresh_enabled}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:container:nodejs"
#     name      = "NodeVersion"
#     value     = "${var.nodejs_version}"
#   }
#   ###===================== Application ENV vars ======================###
  # setting {
  #   namespace = "aws:elasticbeanstalk:application:environment"
  #   # name      = "${element(concat(keys(var.env_vars), list(format(var.env_default_key, 0))), 0)}"
  #   name      = "${element(concat(keys(var.env_vars)))}"
  #   # value     = "${lookup(var.env_vars, element(concat(keys(var.env_vars), list(format(var.env_default_key, 0))), 0), var.env_default_value)}"
  #   value     = "${lookup(var.env_vars, element(concat(keys(var.env_vars))))}"
  # }
#   ###===================== Application Load Balancer Health check settings =====================================================###
#   # The Application Load Balancer health check does not take into account the Elastic Beanstalk health check path
#   # http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environments-cfg-applicationloadbalancer.html
#   # http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environments-cfg-applicationloadbalancer.html#alb-default-process.config
  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "HealthCheckPath"
    value     = "${var.healthcheck_path}"
    #value     = "/isAlive"
  }
  # setting {
  #   namespace = "aws:elasticbeanstalk:environment:process:default"
  #   name      = "Port"
  #   #value     = "80"
  #   value     = "443"
  # }
  # setting {
  #   namespace = "aws:elasticbeanstalk:environment:process:default"
  #   name      = "Protocol"
  #   #value     = "HTTP"
  #   value     = "HTTPS"
  # }


#   ###===================== Notification =====================================================###

#   setting {
#     namespace = "aws:elasticbeanstalk:sns:topics"
#     name      = "Notification Endpoint"
#     value     = "${var.notification_endpoint}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:sns:topics"
#     name      = "Notification Protocol"
#     value     = "${var.notification_protocol}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:sns:topics"
#     name      = "Notification Topic ARN"
#     value     = "${var.notification_topic_arn}"
#   }
#   setting {
#     namespace = "aws:elasticbeanstalk:sns:topics"
#     name      = "Notification Topic Name"
#     value     = "${var.notification_topic_name}"
#   }
#   depends_on = ["aws_security_group.default"]
# }

# data "aws_elb_service_account" "main" {}

# data "aws_iam_policy_document" "elb_logs" {
#   statement {
#     sid = ""

#     actions = [
#       "s3:PutObject",
#     ]

#     resources = [
#       "arn:aws:s3:::${module.label.id}-logs/*",
#     ]

#     principals {
#       type        = "AWS"
#       identifiers = ["${data.aws_elb_service_account.main.arn}"]
#     }

#     effect = "Allow"
#   }
}