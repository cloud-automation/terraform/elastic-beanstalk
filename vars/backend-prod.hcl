bucket = "terraform-state-bucket-prod"
dynamodb_table = "terraform-state-prod"          # Terraform assumes this table is in same account as runner
region = "eu-west-1"
encrypt = "true"