bucket = "terraform-state-bucket-nonprod"
dynamodb_table = "terraform-state-nonprod"          # Terraform assumes this table is in same account as runner
region = "eu-west-1"
encrypt = "true"