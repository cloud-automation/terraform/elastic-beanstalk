# Elastic Beanstalk

![Alt-Text](images/ElasticBeanstalkSeparationOfDuties.png)

This deploys an EB application. The repository name will be the name of the application, and deploys both a nonprod and prod trough the pipeline.
Terraform creates a plan in nonprod and perform some basic code checks, you have to inspect this plan and deploy it to its respective environment (see [Instructions for Application users to trigger a new build](#trigger-a-new-build).

# Table of Contents  

**[Elastic Beanstalk](#elastic-beanstalk)**<br>
**[Table of Contents](#table-of-contents)**<br>
**[Critera for choosing Elastic Beanstalk](#critera-for-choosing-elastic-beanstalk)**<br>
**[Targeted audience for Elastic Beanstalk](#targeted-audience-for-elastic-beanstalk)**<br>
**[Supported Software versions](#supported-software-versions)**<br>
**[Instructions for initial setup](#instructions-for-initial-setup)**<br>
**[Instructions for Application users](#instructions-for-application-users)**<br>
**[Trigger a new build](#trigger-a-new-build)**<br>
**[URL to access application](#url-to-access-application)**<br>
**[Integrate Elastic Beanstalk with new app releases](#integrate-elastic-beanstalk-with-new-app-releases)**<br>
**[Service features and limitations](#service-features-and-limitations)**<br>
**[Cost of running Elastic Beanstalk](#cost-of-running-elastic-beanstalk)**<br>
**[Remove application](#remove-application)**<br>

## Critera for choosing Elastic Beanstalk

### Targeted audience for Elastic Beanstalk

Those who want to deploy and manage their applications within minutes in the AWS Cloud. You don’t need experience with cloud computing to get started. AWS Elastic Beanstalk supports Java Springboot and Tomcat applications.
The target audience for this is Developement teams handing over application (JAR/WAR) artifacts. These artifacts is then complimented with [Elastic Beanstalk specific settings](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/java-tomcat-platform-directorystructure.html) and uploaded.

#### Supported Software versions

We will run frequent patch schedules. Support will be available for the two latest software/OS configurations supplied by AWS. Read [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.java) to stay updated with the latest supported versions.

## Instructions for initial setup

The application name will be the same as the repository name, adding the respective environments domain to the CNAME see [URL to access application](#url-to-access-application).
Copy the boilerplate to the Elastic Beanstalk group with the [connected group runner](https://docs.gitlab.com/ee/ci/runners/). Example:

* Check out this repo locally

```shell
git clone https://gitlab.com/cloud-automation/terraform/elastic-beanstalk.git
```

* Make a branch for your new application and copy the tomcat example as app specific vars file

```shell
git checkout -b my-tomcat-app                                                           # Create new branch
cp applications/eb-tomcat-v3.tfvars applications/my-tomcat-app.tfvars                   # Copy the tomcat example file to application vars file
sed -i 's/CostCenter    = \"0000\"/CostCenter    = \"1234\"/g' applications/my-tomcat-app.tfvars    # Change Cost Center to 1234
sed -i 's/ApplicationID = \"AP0001\"/ApplicationID = \"AP1234\"/1' /my-tomcat-app.tfvars            # Change Application ID to AP1234
git add applications/my-tomcat-app.tfvars
git commit applications/my-tomcat-app.tfvars -m "First commit for the My Tomcat App"
```

* Create the new repo for your new application using git remote.

```shell
git remote -v                                                                           # Show origin remote
git remote add my-tomcat-app https://gitlab.com/cloud-automation/terraform/elastic-beanstalk/my-tomcat-app.git
git remote -v                                                                           # Display the newly added remote
git push --set-upstream my-tomcat-app local-branch:master                               # Deploy / create the new application. Push the 'local-branch' to 'master' on remote
```

### Instructions for Application users

The application team can trigger a new pipeline run with the CI trigger below, but they cannot change the infrastructure code. This is an important feture to understand to have separation of duties as illustrated in [this picture](images/ElasticBeanstalkSeparationOfDuties.png).

#### Trigger a new build

* Create a CI Pipeline trigger (settings>CI/CD>Pipeline triggers).
* Take note of the project number to change the example in this README.md from 9876 to a working one 1234 in this example):

```shell
git checkout -b README-CI-Token-example                                                 # Create new branch from master
sed -i 's%v4/projects/9876/trigger/pipeline%v4/projects/1234/trigger/pipeline%g' README.md  # Update Project number
sed -i 's%boilerplate-url%my-tomcat-app%g' README.md                                    # Also update the URL
git commit README.md -m "Updated README.md with working CI Token example"
git checkout master                                                                     # Go back to master branch
git merge README-CI-Token-example                                                       # Merge the changes
git push --set-upstream my-tomcat-app master:master                                     # Push upstream to GitLab
```

* Execute a build with curl, set ACTION to deploy-nonprod:

```shell
curl -X POST -F token=<token> -F ref=master -F "variables[ACTION]=deploy-nonprod" https://gitlab.com/cloud-automation/api/v4/projects/9876/trigger/pipeline
```

#### URL to access application

You need to host a Route53 domain in the same account you deploy (mydomain.com in this example). This domain should be defined in environments/test.tfvars

*Test / Non-Prod:*  [http://boilerplate-url.mydomain.com/](https://my-tomcat-app.test.mydomain.com/)<br>
*Production:*   [http://boilerplate-url.mydomain.com/](https://my-tomcat-app.mydomain.com/)<br>

### Integrate Elastic Beanstalk with new app releases

When the application team has a new version of their application they need to trigger a new EB deployment.

* Modify helper-scripts/download-eb-app.sh to download your articact (should have name ```${CI_PROJECT_NAME}.zip```)
* Add a new trigger by going to your project's [Settings ➔ CI/CD under Triggers](https://gitlab.com/help/ci/triggers/README#adding-a-new-trigger).
* Trigger [revokation is done in a similar manner](https://gitlab.com/help/ci/triggers/README#revoking-a-trigger)
* Give this token to the application team.

After this, go into the new projects [CI Pipeline](https://gitlab.com/cloud-automation/terraform/elastic-beanstalk/my-tomcat-app/pipelines) to deploy application.
Using this trigger the development application team can deploy or destroy the infrastructure. However unless they have access to the repository they can only modify the application and Elastic Beanstalk settings in [.ebextentions](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/ebextensions.html).

## Service features and limitations

The repository name may only contain letters used in DNS:

 ```No capital, alphanumeric characters```

<b> No application instance is backed up!!!</b> In the event of failure the application ZIP file will be re-deployed to a new server instance, so do not rely on files saved in the intance et cetera.

Any data has to be persisted in S3, a database or similar in the application layer. There are many code examples of how to do this with boto3 libraries.

### Cost of running Elastic Beanstalk

There is no extra costs of running Elastic Beanstalk (EB) itself. However the resources used for EB depends on the underlying resources such as Load Balancers, Databases, EC2 (server) instances and data transfers.
More details is found in the [supplier documentation](https://aws.amazon.com/elasticbeanstalk/pricing/)

The [application source bundle](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/applications-sourcebundle.html) must:

* Consist of a single ZIP file or WAR file (you can include multiple WAR files inside your ZIP file)
* Not exceed 512 MB
* Not include a parent folder or top-level directory (subdirectories are fine)

## Remove application

* Destroy job in pipeline. Beware! This destroy the whole application + data.
* Rename (to archived-reponame) and then archive GitLab project (not the boilerplate repo)
* Remove the app-specific remote from elastic-beanstalk boilerplate
    ```git remote remove my-tomcat-app```