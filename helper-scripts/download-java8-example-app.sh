#!/bin/sh
# Download a sample Tomcat application and rename it to <git_project_name>.zip
# See more examples at https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/tutorials.html

if [ ! -f "${CI_PROJECT_NAME}.zip" ]
    then
        # wget https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/samples/java-tomcat-v3.zip
        # mv java-tomcat-v3.zip ${CI_PROJECT_NAME}.zip
        wget https://github.com/awslabs/eb-java-scorekeep/releases/download/v1.5/eb-java-scorekeep-v1.zip
        mv eb-java-scorekeep-v1.zip "${CI_PROJECT_NAME}.zip"
    else
        echo "${CI_PROJECT_NAME}.zip is already on disk"
    fi
