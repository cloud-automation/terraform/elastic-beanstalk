#!/bin/bash
# Use this to generate a new table of contents in README.md when you've added headlines

grep ^# README.md | grep -v 'Table of Contents' | sed 's/^#* //g'| while read h; do echo "**[$h](#$(echo $h | tr '[:upper:]' '[:lower:]'| sed 's/ /-/g'))**<br>" ; done