#!/bin/sh
# Download an application and package it to <git_project_name>.zip
# See more examples at https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/tutorials.html

# Files here will be put in the root (/) folder on the Elastic Beanstalk EC2
mkdir -p application-bundle
cd application-bundle
# aws s3 sync s3://elasticbeanstalk-my-tomcat-app-bundle/ ./
wget https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/samples/java-tomcat-v3.zip                 # Use example application
unzip java-tomcat-v3.zip && rm java-tomcat-v3.zip
rm -f ../${CI_PROJECT_NAME}-*.zip
zip -r ../${CI_PROJECT_NAME}-${CI_COMMIT_SHA}-${CI_PIPELINE_ID}.zip * .ebextensions                    # Zip everything from S3 bucket to application bundle zip file