# this file is to create the application and it is the first step in Elastic Beanstalk
resource "aws_elastic_beanstalk_application" "mybean" {
  name          = "${var.CI_PROJECT_NAME}"
  description   = "IUAH EB, deployed from ${var.CI_PROJECT_PATH} ${var.CI_COMMIT_SHA}"
  appversion_lifecycle {
    service_role          = "${aws_iam_role.service.arn}"
    # max_count             = 128
    # delete_source_from_s3 = true
  }
}