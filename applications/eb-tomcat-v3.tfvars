###                     ###
## Application specifics ##
###                     ###

# This file is included as vars/<GitLab Project Name>.tfvars

###
###
## Solution Stacks
## See https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html for options
###
SolutionStackName = "64bit Amazon Linux 2018.03 v3.0.7 running Tomcat 8.5 Java 8"

healthcheck_path  = "/"
tags              = {
    CostCenter    = "0000"
    ApplicationID = "AP0000"
    }


# SSH Configuration
ssh_listener_enabled    = "false"
ssh_source_restriction  = "10.199.99.0/24"                             # Allow SSH from this subnet