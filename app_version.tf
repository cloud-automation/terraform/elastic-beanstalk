# This code takes the app code from s3 and deploy it to the web server
# https://www.terraform.io/docs/providers/aws/r/elastic_beanstalk_application_version.html
resource "aws_elastic_beanstalk_application_version" "elasticbeanstalk-appversion" {
  depends_on    = ["aws_s3_bucket_object.eb_app_bundle"]                                      # Wait until S3 app bundle is available
  name          = "${var.CI_COMMIT_SHA}"                                                      # A unique name for the this Application Version. 
  application   = "${var.CI_PROJECT_NAME}"                                                    # Name of the app (same as GIT project folder)
  description   = "application version created by terraform"
  #bucket      = "elasticbeanstalk-${var.region}-${var.CI_PROJECT_NAME}"                     # S3 bucket that contains the Application Version source bundle. 
  bucket        = "${aws_s3_bucket.elasticbeanstalk-bucket.id}"       
  key           = "${var.eb_app_key}"                                                   # S3 object that is the Application Version source bundle. 
}
