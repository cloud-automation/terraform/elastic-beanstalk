###
## Service
##

### IAM ROLES:
resource "aws_iam_role" "service" {
  name               = "elasticbeanstalk-${var.region}-${var.CI_PROJECT_NAME}-service"
  assume_role_policy = "${file("aws_elasticbeanstalk_service_role.json")}"
  tags               = "${var.tags}"
}

resource "aws_iam_role" "ec2-role" {
  name               = "elasticbeanstalk-${var.region}-${var.CI_PROJECT_NAME}-ec2-role"
  assume_role_policy = "${file("aws_elasticbeanstalk_ec2-role.json")}"
  tags               = "${var.tags}"
}

### IAM PROFILES:

resource "aws_iam_instance_profile" "service_instance_profile" {
  name = "${aws_iam_role.service.name}"
  role = "${aws_iam_role.service.name}"
}


resource "aws_iam_instance_profile" "ec2-role" {
  name = "${aws_iam_role.ec2-role.name}"
  role = "${aws_iam_role.ec2-role.name}"
}

### IAM POLICYS ATTACHEDMENTS:

resource "aws_iam_role_policy_attachment" "AWSElasticBeanstalkWebTier" {
  role       = "${aws_iam_role.ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

resource "aws_iam_role_policy_attachment" "AWSElasticBeanstalkMulticontainerDocker" {
  role       = "${aws_iam_role.ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
}

resource "aws_iam_role_policy_attachment" "AWSElasticBeanstalkWorkerTier" {
  role       = "${aws_iam_role.ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
}

resource "aws_iam_role_policy_attachment" "AmazonEC2RoleforSSM" {
  role       = "${aws_iam_role.ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}
### IAM POLICYS:

resource "aws_iam_policy" "elasticbeanstalk_service_role_policy" {
  name      = "${aws_iam_role.service.name}-policy"
  policy   = "${file("aws_elasticbeanstalk_service_role_policy.json")}"
}

resource "aws_iam_role_policy_attachment" "service" {
  role       = "${aws_iam_role.service.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
}

###
## Enhanced health role
###

resource "aws_iam_role_policy_attachment" "enhanced-health" {
  role       = "${aws_iam_role.service.name}"
  policy_arn = "${aws_iam_policy.elasticbeanstalk_service_role_policy.arn}"
}