assume_role = "arn:aws:iam::123456789:role/gitlab-iuah-elasticbeanstalk-deploy"

BackendS3Bucket = "terraform-state-base-infra-prod"  # Use this S3 bucket as state backend
dynamodb_table = "terraform-state-base-infra-prod"   # The name of a DynamoDB table to use for state locking and consistency. The table must have a primary key named LockID


################### VPC Settings ###################
# env.tf and aws_cloudformation_export.tf assumes a CF VPC called gov-vpc is created
# with outputs such as PrivateSubnet1AID.
# Modify accordingly if you have implemented your VPC differently!
ELBScheme  = "internal"                                             # internal load balancer in VPC 
AssociatePublicIpAddress = "false"

################### Load Balancer Settings #########
LoadBalancerType = "application"
ListenerEnabled = "true"
Protocol = "HTTP"

################## DNS / ROUTE53 ##########
domain_name = "mydomain.com"

################## Auto Scaling Settings ##########
InstanceType = "t2.micro"
MinSize      = "1"
MaxSize      = "1"
